import * as $ from 'jquery'

import Post from '@models/Post'
import './babel'
import './styles/styles.css'
import './styles/scss.scss'
// import json from './assets/json'
// import xml from './assets/data.xml'
// import csv from './assets/data.csv'
import WebpackLogo from '@/assets/webpack-logo.png'

import React from 'react'
import {render} from 'react-dom'

const post = new Post('New Post', WebpackLogo);

$('pre').addClass('code').html(post.toString());

const App = () => {
  return <>
    <div className="container">
      WebPack YES
    </div>

    <hr/>

    <div className="logo"/>

    <pre />

    <hr/>

    <div className="box">
      <h2>SCSS</h2>
    </div>
  </>
}


render(<App/>, document.getElementById('root'));

console.log('-----> ', post.toString());
// console.log('-----> ', 'Json: ', json);
// console.log('-----> ', 'Xml ', xml);
// console.log('-----> ', 'Csv ', csv);