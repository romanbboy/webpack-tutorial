async function start(){
  return await Promise.resolve('Async await func')
}

start().then(res => console.log(res, 'dddd'));

class Util{
  static id = Date.now();
}

console.log('-----> ', Util.id);

import('lodash').then(_ => { //это lazy loading, динамическая подгрузка ОТДЕЛЬНОГО (типа 0.js) чанка
  console.log('-----> ', 'lodash: ', _.random(0,42,true));
})