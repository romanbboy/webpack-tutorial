const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const TerserWebpackPlugin = require('terser-webpack-plugin');

const isDev = process.env.NODE_ENV === 'development';
const isProd = process.env.NODE_ENV === 'production';

const optimization = () => {
  const config = {
    splitChunks: {
      chunks: "all"
    }
  }

  if(isProd){
    config.minimizer = [
      new OptimizeCssAssetWebpackPlugin(),
      new TerserWebpackPlugin()
    ]
  }

  return config;
};

const filename = ext => isDev ? `assets/style/[name].${ext}` : `assets/style/[name][hash].${ext}`;

const cssLoaders = extra => {
  const loaders = [
    {
      loader: MiniCssExtractPlugin.loader,
      options: {
        hmr: isDev,
        reloadAll: true
      }
    },
    'css-loader'
  ];

  if(extra) loaders.push(extra);

  return loaders;
}

const babelOptions = preset => {
  const opts = {
    presets: [
      '@babel/preset-env'
    ],
      plugins: [
      '@babel/plugin-proposal-class-properties'
    ]
  }

  if(preset) opts.presets.push(preset);

  return opts;
}

module.exports = {
  context: path.resolve(__dirname, 'src'),
  mode: 'development',
  entry: {
    main: ['@babel/polyfill','./index.jsx'],  // @babel/polyfill - нужен чисто для того, что бы работал async/await
    analytics: './analytics.ts'
  },
  output: {
    filename: filename('js'),
    path: path.resolve(__dirname, 'dist')
  },
  resolve: {
    extensions: ['.js', '.json', '.png'], // какие расширения он понимать будет по умолчанию (их при импорте, в конце названия файла можно не писать)
    alias: {  // это сокращения до нужного пути
      '@models': path.resolve(__dirname, 'src/models'),
      '@': path.resolve(__dirname, 'src')
    }
  },
  // это выносит код, который подключается в разных местах один и тот же (например jquery) в отдельный файл для оптимизации и уменьшения размера итоговых файлов
  optimization: optimization(),
  // это нужно для автоматической перезагрузки страницы (это от пакета webpack-dev-server)
  // ничего в папке dist не будет, если так запускать, потому что он хранит файлы в оперативке, так нужно ТОЛЬКО для development
  devServer: {
    port: 4200,
    hot: isDev
  },
  devtool: isDev ? 'source-map' : '',
  // plugins нужны для подключения библиотек (инстансов классов) для дополнительных возможностей
  plugins: [
    new HTMLWebpackPlugin({
      template: './index.html',
      minify: {
        collapseWhitespace: isProd
      }
    }),
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, 'src/favicon.ico'),
          to: path.resolve(__dirname, 'dist')
        }
      ]
    }),
    new MiniCssExtractPlugin({
      filename: filename('css'),
    })
  ],
  // webpack сделан чисто для js, поэтому что бы работать с другими файлами типа css, img и т.д, то нужно испольщовать лоадеры (как ниже)
  module: {
    rules: [
      // если в качестве import (в коде), нам встречаются файлы отличные от js, то описываем ниже лоадеры для определенных файлов
      {
        // когда в import находит файл .css, то используем определенный лоадер для них
        test: /\.css$/,
        // порядок запуска лоадеров важен, в вебпаке они идут справа на лево (сначала запустится css-loader)
        use: cssLoaders()
      },
      {
        test: /\.s[ac]ss$/,
        use: cssLoaders('sass-loader')
      },
      {
        test: /\.(png|jpg|svg|gif)$/,
        use: ['file-loader']
      },
      {
        test: /\.(ttf|woff|woff2|eot)$/,
        use: ['file-loader']
      },
      {
        test: /\.xml$/,
        use: ['xml-loader']
      },
      {
        test: /\.csv$/,
        use: ['csv-loader']
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: {
          loader: 'babel-loader',
          options: babelOptions()
        }
      },
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        loader: {
          loader: 'babel-loader',
          options: babelOptions('@babel/preset-typescript')
        }
      },
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        loader: {
          loader: 'babel-loader',
          options: babelOptions('@babel/preset-react')
        }
      }
    ]
  }
};